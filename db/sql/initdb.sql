-- Crear el tipo ENUM "users_role_enum"
CREATE TYPE users_role_enum AS ENUM ('User', 'Admin');

-- Crear la tabla "users"
CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL,
    "lastName" VARCHAR NOT NULL,
    address VARCHAR NOT NULL,
    phone_number VARCHAR NOT NULL,
    email VARCHAR NOT NULL,
    password VARCHAR NOT NULL,
    profile_picture VARCHAR NOT NULL DEFAULT '',
    role users_role_enum NOT NULL DEFAULT 'User',
    created_at TIMESTAMP NOT NULL DEFAULT now(),
    updated_at TIMESTAMP NOT NULL DEFAULT now(),
    is_active BOOLEAN NOT NULL DEFAULT false,
    is_deleted BOOLEAN NOT NULL DEFAULT false
);
