import { JwtAuthGuard } from "./../auth/guards/jwt-auth.guard";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from './dto/update-user.dto';
import { UserService } from "./user.service";
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Request,
  UseGuards,
} from "@nestjs/common";
import { ChangePasswordDto } from "./dto/change-password.dto";
import RoleGuard from "../auth/guards/role.guard";
import { Role } from "./role.enun";

@Controller("users")
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post("/register")
  async register(@Body() userDto: CreateUserDto) {
    return this.userService.create(userDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get("/me")
  async getMe(@Request() request) {
    const user = request.user;
    return this.userService.findByIdNoPassword(user.id);
  }

  @UseGuards(JwtAuthGuard)
  @Patch('/me')
  async updateProfile(@Request() request, @Body() updateUserDto: UpdateUserDto) {
    const user = request.user;
    return this.userService.updateProfile(user.id, updateUserDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete("/me")
  async deleteMe(@Request() request) {
    const user = request.user;
    return this.userService.deleteUser(user.id);
  }

  @UseGuards(RoleGuard(Role.Admin))
  @Get()
  async getUsers() {
    return this.userService.getUsers();
  }

  @UseGuards(RoleGuard(Role.Admin))
  @Get("/:id")
  async findById(@Param("id", ParseIntPipe) id: number) {
    return this.userService.findByIdNoPassword(id);
  }

  @Post("/reset-password")
  async resetPassword(@Body() resetPasswordBody: { email: string }) {
    return this.userService.resetPassword(resetPasswordBody.email);
  }

  @UseGuards(JwtAuthGuard)
  @Patch("/change-password")
  async changeMyPassword(
    @Request() request,
    @Body() changePasswordDto: ChangePasswordDto
  ) {
    const user = request.user;
    return this.userService.changePassword(user.id, changePasswordDto);
  }

  @UseGuards(RoleGuard(Role.Admin))
  @Patch("/change-password/:id")
  async changePasswordById(
    @Param("id") id,
    @Body() changePasswordDto: ChangePasswordDto
  ) {
    return this.userService.changePassword(id, changePasswordDto);
  }

  @UseGuards(RoleGuard(Role.Admin))
  @Delete("/:id")
  async deleteUserById(@Param("id") id: string) {
    return this.userService.deleteUser(id);
  }
}
